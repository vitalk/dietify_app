import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import Login from '@/pages/Login'
import Signup from '@/pages/Signup'
import Menu from '@/pages/Menu'
import Diet from '@/pages/Diet'
import Stats from '@/pages/Stats'
import Barcode from '@/pages/Barcode'
import Profile from '@/pages/Profile'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { auth: false }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { auth: false }
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
      meta: { auth: false }
    },
    {
      path: '/menu',
      name: 'menu',
      component: Menu,
      meta: { auth: true }
    },
    {
      path: '/diet',
      name: 'diet',
      component: Diet,
      meta: { auth: true }
    },
    {
      path: '/stats',
      name: 'stats',
      component: Stats,
      meta: { auth: true }
    },
    {
      path: '/barcode',
      name: 'barcode',
      component: Barcode,
      meta: { auth: true }
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: { auth: true }
    }
  ]
})
