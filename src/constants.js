const DietType = {
  HEALTY_FOOD: 1,
  DIABET_1: 2,
  DIABET_2: 3,
  LOOSE_WEIGHT: 4,
  GAIN_WEIGHT: 5
}

const dietTypeOptions = [
  { label: 'Healty food', value: DietType.HEALTY_FOOD },
  { label: 'Diabet 1', value: DietType.DIABET_1 },
  { label: 'Diabet 2', value: DietType.DIABET_2 },
  { label: 'Loose weight', value: DietType.LOOSE_WEIGHT },
  { label: 'Gain weight', value: DietType.GAIN_WEIGHT }
]

const ScheduleType = {
  BREAKFAST: 1,
  LUNCH: 2,
  DINNER: 3,
  SNACK: 4
}

const scheduleTypeOptions = [
  { label: 'Breakfast', value: ScheduleType.BREAKFAST },
  { label: 'Lunch', value: ScheduleType.LUNCH },
  { label: 'Dinner', value: ScheduleType.DINNER },
  { label: 'Snack', value: ScheduleType.SNACK }
]

const LifeStyleType = {
  PASSIVE: 1,
  SLIGHT_ACTIVITY: 2,
  AVERAGE_ACTIVITY: 3,
  HIGH_ACTIVITY: 4,
  VERY_HIGH_ACTIVITY: 5
}

const lifeStyleTypeOptions = [
  { label: 'Passive', value: LifeStyleType.PASSIVE },
  { label: 'Slight activity', value: LifeStyleType.SLIGHT_ACTIVITY },
  { label: 'Average activity', value: LifeStyleType.AVERAGE_ACTIVITY },
  { label: 'High activity', value: LifeStyleType.HIGH_ACTIVITY },
  { label: 'Very high activity', value: LifeStyleType.VERY_HIGH_ACTIVITY }
]

const GenderType = { // eslint-disable-line
  MEN: 1,
  WOMEN: 2
}

const genderTypeOptions = [
  { label: 'Men', value: GenderType.MEN },
  { label: 'Women', value: GenderType.WOMEN }
]

export default {
  ScheduleType,
  scheduleTypeOptions,
  DietType,
  dietTypeOptions,
  LifeStyleType,
  lifeStyleTypeOptions,
  GenderType,
  genderTypeOptions
}
